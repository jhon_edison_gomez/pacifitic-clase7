#!/bin/bash

touch ./webpack.config.js;

mkdir ./src;
mkdir ./dist;

mkdir ./src/app;
mkdir ./src/assets;

touch ./src/index.html;
touch ./src/main.js;
touch ./src/main.ts;
touch ./src/main.scss;
